from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


from . import models
# Create your views here.
class BookListView(LoginRequiredMixin, ListView):
    model = models.Book
    
    template_name = 'books/book_list.html'
    context_object_name = "books"
    login_url = "account_login"

class BookDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = models.Book
    template_name = 'books/book_details.html'
    context_object_name = "book"
    login_url = "account_login"
    permission_required = ('books.special_status',)
    # permission_required = ()