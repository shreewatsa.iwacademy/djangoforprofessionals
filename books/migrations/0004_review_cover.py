# Generated by Django 2.2.7 on 2020-03-14 02:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0003_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='review',
            name='cover',
            field=models.ImageField(blank=True, upload_to='books/covers/'),
        ),
    ]
