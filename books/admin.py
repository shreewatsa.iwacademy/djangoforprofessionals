from django.contrib import admin

# Register your models here.
from . import models

class ReviewInline(admin.TabularInline):
    model = models.Review

class BookAdmin(admin.ModelAdmin):
    inlines = [
        ReviewInline,
    ]
    list_display = ['title','author','price',]

admin.site.register(models.Book, BookAdmin)