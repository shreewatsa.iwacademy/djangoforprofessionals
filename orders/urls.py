from django.urls import path, include

from . import views

app_name = "orders"

urlpatterns = [
    path('', views.OrdersPageView.as_view(), name="orders"),
    path('charge/', views.charge, name="charge"),

]
