import stripe

from django.shortcuts import render
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib.auth.models import Permission

from bookstore_project.loggingConfig import logging
logger = logging.getLogger(__name__)
# Create your views here.
class OrdersPageView(TemplateView):
    template_name = "orders/purchase.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stripe_key'] = settings.STRIPE_TEST_PUBLISHABLE_KEY
        logger.debug(f'Stripe Publishable Key is: {context.get("stripe_key")}')
        return context

stripe.api_key = settings.STRIPE_TEST_SECRET_KEY
def charge(request):
    # permission = Permission.objects.get(codename="special_status")
    # user = request.user
    # user.user_permissions.add(permission)
    if request.method == "POST":
        # breakpoint()
        logger.info(f"stripeToken is : {request.POST.get('stripeToken', None)}")
        charge = stripe.Charge.create(
            amount=4055,
            currency='usd',
            description='Purchase All Books',
            source=request.POST['stripeToken']
        )
        # breakpoint()
        return render(request, "orders/charge.html")