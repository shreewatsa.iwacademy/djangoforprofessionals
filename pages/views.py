from django.shortcuts import render
from django.views.generic import TemplateView

from bookstore_project.loggingConfig import logging
logger = logging.getLogger(__name__)

# Create your views here.
class HomepageView(TemplateView):
    template_name = "pages/home.html"
    def dispatch(self, request, *args, **kwargs):
        logger.debug("Home page is being called.")
        return super().dispatch(request, *args, **kwargs)