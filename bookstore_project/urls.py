"""
    Root URLconf
"""


from django.contrib import admin
from django.urls import path, include

from django.conf import settings                # importing settings
from django.conf.urls.static import static      # for media files

urlpatterns = [
    # Django admin
    path('admin/', admin.site.urls),

    # User Management
    # path('accounts/', include("django.contrib.auth.urls")),           
    # path('accounts/', include("users.urls", namespace="users")),
    path('accounts/', include("allauth.urls")),                     # Using django-allauth urls for login, signup...


    # Local Apps
    path('', include("pages.urls", namespace="pages")),
    path('', include("books.urls", namespace="books")),
    path('orders/', include("orders.urls", namespace="orders")),


]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
